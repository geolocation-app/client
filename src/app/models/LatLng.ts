export interface LatLng {
  latitude: number;
  langitude: number;
}
